/*
 * KatLight 1.0
 * Copyright (c) 2018 Agis Wichert
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * http://www.rohling.de
 */
// Neopixels
#include <Adafruit_NeoPixel.h>
#define NeoPIN D2
#define NUM_LEDS 42
int brightness = 150;
Adafruit_NeoPixel strip = Adafruit_NeoPixel(NUM_LEDS, NeoPIN, NEO_RGB + NEO_KHZ800);
String lastColor = "#000000";
int colorR = 0;
int colorG = 0;
int colorB = 0;

// sunrise 
bool sunriseStarted = false;
int sunriseHour = 0;
int sunriseMinute = 0;
long sunsetTimer = millis();
bool sunsetStarted = false;
long sunsetDuration = 60000 * 2; // * minutes
int sunsetPercentage = 0;

// Neopixel Animation
int animatedPixel = 0;
String fadeColor = "#FFFFFF";
long animationTime = 120; // 120 ms
long animationPos = millis();
bool lampOn = false;

// blue light animation
int blueLightPos = 0;

// blink
int blinkTime = 1000;
bool isBlinkOn = false;

// ESP 8266
#include <ESP8266WiFi.h>
#include <PubSubClient.h>

// ESPName
char* EspName = "KatLight";

// WiFi & MQTT Server
const char* ssid = "<SSID>";
const char* password = "<Password>";
const char* mqtt_server = "192.168.0.77";

WiFiClient espClient;
PubSubClient pubClient(espClient);

// NTP
#include <TimeLib.h>
#include <NtpClientLib.h>
bool NtpStarted = false;

// I2C
#include <Wire.h>
#define SCL D3
#define SDA D4

// Gesture sensor
#define Com1 D5
#define Com2 D6
#define Com3 D7

// BME 280 
#include <Adafruit_Sensor.h>
#include <Adafruit_BME280.h>
Adafruit_BME280 bme;

// MQTT
const char* ModeSubscribe = "katlight/mode/sub";
const char* ModePublish = "katlight/mode/pub";

const char* ColorSubscribe = "katlight/color/sub";
const char* ColorPublish = "katlight/color/pub";

const char* TempPublish = "katlight/temp/pub";
const char* HumPublish = "katlight/hum/pub";

const char* HourSubscribe = "katlight/hour/sub";
const char* HourPublish = "katlight/hour/pub";

const char* MinuteSubscribe = "katlight/minute/sub";
const char* MinutePublish = "katlight/minute/pub";

const char* TimePublish = "katlight/time/pub";

long publishInterval = 1500; // publish every 1500 ms
long lastPublish = 0;

/*
 * Modes:
 * 0 - off
 * 1 - all on
 * 2 - animation
 * 3 - blue light
 * 4 - fire simulation
 * 5 - sparkle
 * 6 - Blink
 * 7 - Sunrise simulation (no color select)
 * 8 - disco light (no color select)
 */
int katMode = 0;


bool stateChanged = false;


void setup() {
  Serial.begin(115200);
  // WiFi
  setup_wifi();

  // NTP
  NtpStarted = false;// NTP.begin("europe.pool.ntp.org", 1, true, 0);
  if(!NtpStarted){
    Serial.println(F("No Time"));
    NTP.stop();
  }
  else{
    NTP.setInterval(67);
  }
  // Neopixel
  strip.setBrightness(brightness);
  strip.begin();
  strip.show();
  delay(50);

  // I2C
  Wire.begin(SDA, SCL);

  // Gesture sensor
  pinMode(Com1, INPUT);
  pinMode(Com2, INPUT);
  pinMode(Com3, INPUT);

  // BME280
  if(!bme.begin(0x76, &Wire)){
    Serial.println(F("Temperature Sensor not found or not operational"));
  }
  else{
    Serial.println(F("Temperature Sensor operational"));
  }

  // MQTT Server
  Serial.print("Connecting to MQTT...");
  // connecting to the mqtt server
  pubClient.setServer(mqtt_server, 1883);
  pubClient.setCallback(callback);
  Serial.println(F("done!"));
  
  // reset last publish timer
  lastPublish = millis();
}

void loop() {
  if (!pubClient.connected()) {
    delay(100);
    reconnect();
  }
  pubClient.loop();

  // read gesture 
  readGesture();
  // check mode
  checkMode();
  // publish topics
  publishTopics();
}


// MQTT server connect
void reconnect() {
  // try to reconnect
  if (!pubClient.connected()) {
    // Attempt to connect
    if (pubClient.connect(EspName)) {
      Serial.println(F("connected"));

      // subsrcibe to mode
      pubClient.subscribe(ModeSubscribe);
      pubClient.publish(ModePublish, "0");
      pubClient.loop();

      // subsrcibe to color
      pubClient.subscribe(ColorSubscribe);
      pubClient.publish(ColorPublish, "#000000");
      pubClient.loop();

      // publish temperature
      pubClient.publish(TempPublish, "0");
      pubClient.loop();

      // publish humidity
      pubClient.publish(HumPublish, "0");
      pubClient.loop();

      // subsrcibe to hour
      pubClient.subscribe(HourSubscribe);
      pubClient.publish(HourPublish, "0");
      pubClient.loop();
      
      // subsrcibe to minute
      pubClient.subscribe(MinuteSubscribe);
      pubClient.publish(MinutePublish, "0");
      pubClient.loop();

      // publish time
      Serial.print(F("Publish time..."));
      pubClient.publish(TimePublish, "0");
      pubClient.loop();
      Serial.println(F("Done"));
      
    } else {
      Serial.print(F("failed, rc="));
      Serial.print(pubClient.state());
      Serial.println(F(" try again in 5 seconds"));
      // Wait 5 seconds before retrying
      delay(5000);
    }
  }
}


// Network connection
void setup_wifi() {
  delay(10);
  // We start by connecting to a WiFi network
  Serial.println();
  Serial.print("Connecting to ");
  Serial.println(ssid);
  WiFi.mode(WIFI_STA);
  WiFi.begin(ssid, password);

  while (WiFi.status() != WL_CONNECTED) {
    delay(100);
    Serial.print(".");
  }

  Serial.println("");
  Serial.println(F("WiFi connected"));
  Serial.println(F("IP address: "));
  Serial.println(WiFi.localIP());
}

// read the gesture from an external controller
void readGesture(){
  int in1 = digitalRead(Com1);
  int in2 = digitalRead(Com2);
  int in3 = digitalRead(Com3);

  int sum = in1 * pow(2, 0);
  sum += in2 * pow(2, 1);
  sum += in3 * pow(2, 2);
  
  if(in1 == HIGH && in2 == LOW && in3 == LOW){
    // down
    // just on/off
    if(katMode == 0){
      if(lastColor.equals("#000000")){
        lastColor = fadeColor;
      }
      katMode = 1;
    }
    else{
      katMode = 0;
    }
    stateChanged = true;
    Serial.println(F("Down"));
    Serial.print("Sum: ");
    Serial.println(sum, DEC);
  }
  else if( in1 == LOW && in2 == HIGH && in3 == LOW){
    // up
    // just on/off
    if(katMode == 0){
      if(lastColor.equals("#000000")){
        lastColor = fadeColor;
      }
      katMode = 1;
    }
    else{
      katMode = 0;
    }
    stateChanged = true;
    Serial.println(F("UP"));
    Serial.print(F("Sum: "));
    Serial.println(sum, DEC);
  }
  else if( in1 == HIGH && in2 == HIGH && in3 == LOW){
    // left
    // just on/off
    if(katMode == 0){
      if(lastColor.equals("#000000")){
        lastColor = fadeColor;
      }
      katMode = 1;
    }
    else{
      katMode = 0;
    }
    stateChanged = true;
    Serial.println(F("LEFT"));
    Serial.print("Sum: ");
    Serial.println(sum, DEC);
  }
  else if( in1 == LOW && in2 == LOW && in3 == HIGH){
    // right
    // just on/off
    if(katMode == 0){
      if(lastColor.equals("#000000")){
        lastColor = fadeColor;
      }
      katMode = 1;
    }
    else{
      katMode = 0;
    }
    stateChanged = true;
    Serial.println(F("RIGHT"));
    Serial.print("Sum: ");
    Serial.println(sum, DEC);
  }
  else{
    // no gesture
  }
}


void checkMode(){
  if(stateChanged){
    //Serial.println(F("Mode Changed"));
    if(katMode == 0){
      // all off
      setNeoColor("#000000");
      stateChanged = false;
      lampOn = false;
    }
    else if(katMode == 1){
      // all on
      setNeoColor(lastColor);
      stateChanged = false;
    }
    else if(katMode == 2){
      // animateLight
      animateLight();
    }
    else if(katMode == 3){
      // blue light animation
      blueLight();
    }
    else if(katMode == 4){
      // simulated flames
      flameSimulator();
    }
    else if(katMode == 5){
      // light with sparkles
      sparkle();
    }
    else if(katMode == 6){
      // blink
      blinking();
    }
    else if(katMode == 7){
      // sunrise alarm
      Serial.println(NTP.getTimeDateString());
      // sunrise mode
      if(sunsetPercentage < 100){
        sunrise();
      }
      else {
        lastColor = "#FFFFFF";
        katMode = 1;
        sunsetPercentage = -1;
        Serial.println(F("Sunrise stopped"));
      }
    }
    else if(katMode == 8){
      // disco light
      discoLight();
    }
  }
}

// converting string color to rgb color
void loadColor(String value){
    int number = (int) strtol( &value[1], NULL, 16);
    colorR = number >> 16;
    colorG = number >> 8 & 0xFF;
    colorB = number & 0xFF;
}

// #############
// #region modes

// (2) snake animation
void animateLight(){
  if((animationPos + animationTime) < millis()){
    if(animatedPixel < NUM_LEDS){
      // turn next pixel on
      strip.setPixelColor(animatedPixel, strip.Color( colorG, colorR, colorB ) );
      strip.show();
      animatedPixel++;
      animationPos = millis();
    }
    else{
      // end animation, restart
      animatedPixel = 0;
      animationPos = millis();
      String tmpColor = lastColor;
      lastColor = fadeColor;
      fadeColor = tmpColor;
      loadColor(lastColor);
    }
  }
}

// (3) blue-light animation
void blueLight(){
  if((animationPos + animationTime) < millis()){
    // clear all 
    setNeoColor("#000000");
    // load color
    loadColor(lastColor);
    // try setting one column
    for(int i = blueLightPos; i < NUM_LEDS; i = i+10){
      strip.setPixelColor(i, strip.Color( colorG, colorR, colorB ) );
    }
    strip.show();
    blueLightPos++;
    if(blueLightPos > 11){
      // start over again
      blueLightPos = 0;
    }
    animationPos = millis();
  }
}

// (4) fire!!!
void flameSimulator(){
  if((animationPos + animationTime) < millis()){
    // random value of leds to light up
    loadColor(lastColor);
    int maxValue = random(0,NUM_LEDS);
    for(int i=0; i < maxValue; i++) {
        strip.setPixelColor(i, strip.Color( colorG, colorR, colorB ) );
    }
    // rest black
    for(int b = maxValue; b < NUM_LEDS; b++){
      strip.setPixelColor(b, strip.Color( 0, 0, 0 ) );
    }
    strip.show();
    animationPos = millis();
  }
}

// (5) sparkeling lamp
void sparkle(){
  if((animationPos + animationTime) < millis()){
    setNeoColor(lastColor);
    for(int i = 0; i < 5; i++){
      int pix = random(0,NUM_LEDS);
      strip.setPixelColor(pix, strip.Color( 254, 254, 254 ) );
    }
    strip.show();
    animationPos = millis();
  }
}

// (6) blink
void blinking(){
  if((animationPos + blinkTime) < millis()){
    if(isBlinkOn){
      // off
      setNeoColor("#000000");
    }
    else{
      // on
      setNeoColor(lastColor);
    }
    isBlinkOn = !isBlinkOn;
    animationPos = millis();
  }
}

// (7) sunrise simulator
void sunrise(){
  if(sunriseStarted){
    int timePassed = millis() - sunsetTimer;
    int percent = (timePassed * 100) / sunsetDuration;
    
    if(percent != sunsetPercentage){
      Serial.println(percent, DEC);
      int r = map(percent, 0, 33, 0, 255);
      int g = map(percent, 33, 66, 0, 255);
      int b = map(percent, 66, 100, 0, 255);
      if(r > 255){
        r = 255;
      }
      if(g < 0){
        g = 0;
      }
      else if (g > 255){
        g = 255;
      }
      if(b < 0 ){
        b = 0;
      }
      else if( b > 255){
        b = 255;
      }
      String col = "#" + toHex(r) + toHex(g) + toHex(b);
      Serial.println(col);
      setNeoColor(col);
      sunsetPercentage = percent;
    }
  }
  else {
    if(NtpStarted){
      time_t stime = NTP.getTime();
      if(sunriseHour == hour(stime)){
        if(sunriseMinute == minute(stime) || sunriseMinute == (minute(stime)-1) ){
          sunriseStarted = true;
          sunsetTimer = millis();
          Serial.println(F("Sunrise started"));
        }
      }
    }
    else{
      sunriseStarted = true;
      sunsetTimer = millis();
      Serial.println(F("Sunrise started - no NTP server!!!"));
    }
  }
}

// setting all Neopixels to one color
void setNeoColor(String value){
    // load color value
    loadColor(value);
    for(int i=0; i < NUM_LEDS; i++) {
      strip.setPixelColor(i, strip.Color( colorG, colorR, colorB ) );
    }
    strip.show();
}

// disco light 
void discoLight(){
  if((animationPos + animationTime) < millis()){
    int led = 0;
    int rRed = random(0,254);
    int rBlue = random(0,254);
    int rGreen = random(0,254);
    for(int i = 0; i < 4; i++){
      led = random(0,NUM_LEDS);
      strip.setPixelColor(led, strip.Color( rGreen, rRed, rBlue ) );
    }
    strip.show();
    animationPos = millis();
  }
}

// #endregion modes
// ################

// ###############
// #region Helpers
String toHex(int val){
  String result = String(val, HEX);
  if(result.length() == 1){
    result = "0" + result;
  }
  return result;
}

String timeToString(time_t val){
  String result = String("");
  int h = hour(val);
  if(hour(val)<10){
    result = result + "0";
  }

  int m = minute(val);
  result = result + String(h);
  result = result + ":";
  if(m<10){
    result = result + "0";
  }
  result = result + String(m);
  return result;
}

// #endregion Helpers
// ##################

// handles the recieved MQTT messages
void callback(char* topic, byte* payload, unsigned int length) {
  Serial.print(F("Message arrived ["));
  Serial.print(topic);
  Serial.print("] !");

  if(String(topic).equals(String(ColorSubscribe))){
    // changing color
    String color("#");
    for (int i = 0; i < length; i++) {
      Serial.print((char)payload[i]);
      if(i > 0){
        color = color + String((char)payload[i]);  
      }
    }
    if(!lastColor.equals(color)){
      Serial.println(F("New Color"));
      stateChanged = true;
    }
    
    if(color.equals("#000000")){
      fadeColor = lastColor;
    }
    else{
      fadeColor = "#000000";
    }
    lastPublish = 0;
    lastColor = color;
    // special mode 2
    if( katMode == 2){
      loadColor(lastColor);
      animatedPixel = 0;
    }
  }
  else if(String(topic).equals(String(ModeSubscribe))){
    // changing mode
    String m("");
    for (int i = 0; i < length; i++) {
        m = m + String((char)payload[i]);
    }
    int newMode = m.toInt();
    if(newMode != katMode){
      stateChanged = true;
    }
    katMode = newMode;
     // special mode 2
    if( katMode == 2){
      loadColor(lastColor);
      animatedPixel = 0;
    }
    sunriseStarted = false;
    Serial.print(m);
  }
  else if(String(topic).equals(String(HourSubscribe))){
    // setting alarm hour
    String m("");
    for (int i = 0; i < length; i++) {
        m = m + String((char)payload[i]);
    }
    sunriseHour = m.toInt();
    
    Serial.print(sunriseHour, DEC);
    Serial.println(" h");
  }
  else if(String(topic).equals(String(MinuteSubscribe))){
    // setting alarm minute
    String m("");
    for (int i = 0; i < length; i++) {
        m = m + String((char)payload[i]);
    }
    sunriseMinute = m.toInt();

    Serial.print(sunriseMinute, DEC);
    Serial.println(" m");
  }
  else{
    Serial.println(F("No fitting topic found"));
  }
  Serial.println();
}

// publish topics to sync with all devices
void publishTopics(){
  
  if(( lastPublish + publishInterval) < millis()){
    // mode
    char b[2];
    String str;
    str=String(katMode);
    str.toCharArray(b,2);
    pubClient.publish( ModePublish, b );  

    // current color
    char msg[7];
    lastColor.toCharArray(msg, lastColor.length()+1);
    pubClient.publish(ColorPublish, msg);

    // publish temperature
    int temp = bme.readTemperature();
    char t[4];
    str=String(temp);
    str.toCharArray(t,4);
    pubClient.publish(TempPublish, t);

    // publish humidity
    temp = bme.readHumidity();
    str=String(temp);
    str.toCharArray(t,4);
    pubClient.publish(HumPublish, t);

    // publish sunriseminute
    char sm[3];
    str=String(sunriseMinute);
    str.toCharArray(sm,3);
    pubClient.publish(MinutePublish, sm);

    // publish sunriseminute
    str=String(sunriseHour);
    str.toCharArray(sm,3);
    pubClient.publish(HourPublish, sm);

    // publish current time
    char timearray[6];
    if(NtpStarted){
      time_t stime = NTP.getTime();
      str = timeToString(stime);
    }
    else{
      str = "NoTime";
    }
    str.toCharArray(timearray,6);
    pubClient.publish(TimePublish, timearray);
    
    // reset pub timer
    lastPublish = millis();
  }
}
