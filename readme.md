# KatLight
(c) 2018 Agis Wichert

[http://www.rohling.de](http://www.rohling.de)


[Project Video on YT](https://youtu.be/cBkrs1prnkw)

[Project Page](https://rohling-de.blogspot.com/2018/09/katlight-wonderlamp.html)


### Channels
**Modes**

"katlight/mode/sub"

"katlight/mode/pub"


**Color**

"katlight/color/sub"

"katlight/color/pub"


**Temperature & Humidity**

"katlight/temp/pub"

"katlight/hum/pub"


**Alarm Hour**

"katlight/hour/sub"

"katlight/hour/pub"


**Alarm Minute**
"katlight/minute/sub"

"katlight/minute/pub"


**Current Time**

"katlight/time/pub"


### Modes

## Off (0)
All LEDs are turned off

## On (1) 
All LEDs are on

## Animation (2) 
Sets the color of the stripe one pixel at a time, starting from the bottom, rising to the top.

## Blue Light (3)
A rotating light. Like the one the firefighter or the police use it. But in any color you want.

## Fire Simulation (4)
Random flicker, that starts at the bottom, rises and falls randomly.

## Sparkle  (5) 
Turns on all LEDs at the desired color. Adds randomly some white sparkles to the light.

## Blink (6)
Remember the classical blink-sketch? Now with a hole lamp and the color you like.

## Sunrise Alarm (7)
You set the hour and minute where the alarm starts and it simulates a sunrise (like in the [sunrise simulator project](https://rohling-de.blogspot.com/2018/07/sunrise-simulator.html)).

## Disco Lights (8)
Randomly sets up to 4 LEDs to a random color.
